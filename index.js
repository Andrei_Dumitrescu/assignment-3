const FIRST_NAME = "Andrei";
const LAST_NAME = "Dumitrescu";
const GRUPA = "1090";

/**
 * Make the implementation here
 */
class Employee {
    constructor (name, surname, salary){
        this.name = name;
        this.surname = surname;
        this.salary = salary;
    }

    getDetails(){
        return this.name + ' ' + this.surname + ' ' + this.salary;
    }
}

const andrei = new Employee ('Andrei', 'Dumitrescu', 5000);
console.log(andrei.getDetails());

class SoftwareEngineer extends Employee {
   constructor (name, surname, salary, experience){
        super(name, surname, salary);
        if(experience === undefined){
            this.experience = 'JUNIOR';
        }else this.experience = experience;
   }
   getDetails(){
    return this.name + ' ' + this.surname + ' ' + this.salary + ' ' + this.experience;

    }
   applyBonus(){
       if(this.experience === 'JUNIOR'){
       this.salary = this.salary * 1.1;
       }else
       if(this.experience === 'MIDDLE'){
        this.salary = this.salary * 1.15;
       }else
       if(this.experience === 'SENIOR'){
        this.salary = this.salary * 1.2;
       } else this.salary = this.salary * 1.1;
        
       return this.salary;
    }
}

const eng = new SoftwareEngineer('Andrei', 'Dumitrescu', 5000, 'SENIOR');
console.log(eng.getDetails());
console.log(eng.applyBonus());
console.log(eng.getDetails());

const eng1 = new SoftwareEngineer('Andrei', 'Dumitrescu', 5000);
console.log(eng1.getDetails());
console.log(eng1.applyBonus());
console.log(eng1.getDetails());

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    Employee,
    SoftwareEngineer
}

